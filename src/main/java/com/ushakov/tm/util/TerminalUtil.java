package com.ushakov.tm.util;

import com.ushakov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() throws IndexIncorrectException {
        final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        }
        catch(Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}

package com.ushakov.tm.controller;

import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.api.controller.ITaskController;
import com.ushakov.tm.api.service.ITaskService;
import com.ushakov.tm.enumerated.Sort;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.exception.system.IndexIncorrectException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public TaskController(ITaskService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks = new ArrayList<>();
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public void create() throws TaskNotFoundException, EmptyNameException, EmptyDescriptionException {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String taskDescription = TerminalUtil.nextLine();
        final Task task = taskService.add(taskName, taskDescription);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
    }

    @Override
    public void removeTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(taskName);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void removeTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = taskService.removeOneByIndex(taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void findTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println(task);
    }

    @Override
    public void findTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(taskName);
        if (task == null) throw new TaskNotFoundException();
        System.out.println(task);
    }

    @Override
    public void findTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
        System.out.println(task);
    }

    @Override
    public void updateTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException, EmptyNameException {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = taskService.findOneByIndex(taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (taskService.updateTaskByIndex(taskIndex - 1, taskName, taskDescription) == null) {
            throw new TaskNotFoundException();
        }
    }

    @Override
    public void updateTaskById() throws TaskNotFoundException, EmptyIdException, EmptyNameException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (taskService.updateTaskById(taskId, taskName, taskDescription) == null) {
            throw new TaskNotFoundException();
        }
    }

    @Override
    public void startTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(taskName);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void startTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = taskService.startTaskByIndex(taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void completeTaskById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskById(taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void completeTaskByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskByName(taskName);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void completeTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        final Task task = taskService.completeTaskByIndex(taskIndex - 1);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusById() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusById(taskId, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusByName() throws TaskNotFoundException, EmptyNameException {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByName(taskName, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void changeTaskStatusByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeTaskStatusByIndex(taskIndex-1, status);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void findAllTasksByProjectId() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> projectTaskList = projectTaskService.findAllTasksByProjectId(projectId);
        if (projectTaskList == null) throw new TaskNotFoundException();
        int index = 1;
        for (final Task task: projectTaskList) {
            System.out.println(index + ": " +task);
            System.out.println();
            index++;
        }
    }

    @Override
    public void bindTaskByProjectId() throws TaskNotFoundException, EmptyIdException, ProjectNotFoundException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskByProjectId(projectId, taskId);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    public void unbindTaskFromProject() throws TaskNotFoundException, EmptyIdException {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(taskId);
        if (task == null) throw new TaskNotFoundException();
    }

}

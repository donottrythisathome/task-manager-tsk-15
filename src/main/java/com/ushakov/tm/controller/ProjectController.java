package com.ushakov.tm.controller;

import com.ushakov.tm.api.controller.IProjectController;
import com.ushakov.tm.api.service.IProjectService;
import com.ushakov.tm.api.service.IProjectTaskService;
import com.ushakov.tm.enumerated.Sort;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.exception.system.IndexIncorrectException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects = new ArrayList<>();
        if (sort == null || sort.isEmpty()) projects = projectService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Project project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @Override
    public void create() throws ProjectNotFoundException, EmptyNameException, EmptyDescriptionException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String projectDescription = TerminalUtil.nextLine();
        final Project project = projectService.add(projectName, projectDescription);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void clear() {
        System.out.println("[PROJECT CLEAR]");
        projectService.clear();
    }

    @Override
    public void removeProjectById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.removeOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeProjectByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.removeOneByName(projectName);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void removeProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        final Project project = projectService.removeOneByIndex(projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void findProjectById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println(project);
    }

    @Override
    public void findProjectByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.findOneByName(projectName);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println(project);
    }

    @Override
    public void findProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println(project);
    }

    @Override
    public void updateProjectByIndex()
            throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException, EmptyNameException {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        final Project project = projectService.findOneByIndex(projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String projectDescription = TerminalUtil.nextLine();
        if (projectService.updateProjectByIndex(projectIndex - 1, projectName, projectDescription) == null) {
            throw new ProjectNotFoundException();
        }
    }

    @Override
    public void updateProjectById() throws ProjectNotFoundException, EmptyIdException, EmptyNameException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("ENTER NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String projectDescription = TerminalUtil.nextLine();
        if (projectService.updateProjectById(projectId, projectName, projectDescription) == null) {
            throw new ProjectNotFoundException();
        }
    }

    @Override
    public void startProjectById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.startProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.startProjectByName(projectName);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void startProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        if (projectIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Project project = projectService.startProjectByIndex(projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void completeProjectById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void completeProjectByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        final Project project = projectService.completeProjectByName(projectName);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void completeProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        final Project project = projectService.completeProjectByIndex(projectIndex - 1);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeProjectStatusById() throws ProjectNotFoundException, EmptyIdException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusById(projectId, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeProjectStatusByName() throws ProjectNotFoundException, EmptyNameException {
        System.out.println("ENTER PROJECT NAME");
        final String projectName = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByName(projectName, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void changeProjectStatusByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException {
        System.out.println("ENTER PROJECT INDEX");
        final Integer projectIndex = TerminalUtil.nextNumber();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = projectService.changeProjectStatusByIndex(projectIndex-1, status);
        if (project == null) throw new ProjectNotFoundException();
    }

    @Override
    public void deleteProjectById() throws ProjectNotFoundException, EmptyIdException, TaskNotFoundException {
        System.out.println("ENTER PROJECT ID");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectTaskService.deleteProjectById(projectId);
        if (project == null) throw new ProjectNotFoundException();
    }

}

package com.ushakov.tm.exception.system;

public class UnknownArgumentException extends Exception {

    public UnknownArgumentException(String value) {
        super("Error! Unknown argument: " + value + "!");
    }

}

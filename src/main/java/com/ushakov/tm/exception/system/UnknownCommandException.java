package com.ushakov.tm.exception.system;

public class UnknownCommandException extends Exception {

    public UnknownCommandException(String value) {
        super("Error! Unknown command: " + value + "!");
    }

}

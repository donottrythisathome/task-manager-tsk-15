package com.ushakov.tm.exception.system;

public class IndexIncorrectException extends Exception {

    public IndexIncorrectException() {
        super("Index is not correct!");
    }

    public IndexIncorrectException(String message) {
        super("Entered value " + message + " is not a number!");
    }

}

package com.ushakov.tm.exception.entity;

public class ProjectNotFoundException extends Exception {

    public ProjectNotFoundException() {
        super("Error! Project was not found!");
    }

}

package com.ushakov.tm.repository;

import com.ushakov.tm.api.repository.ITaskRepository;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(list);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task removeOneByName(final String name) throws TaskNotFoundException {
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneById(final String id) throws TaskNotFoundException {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) throws TaskNotFoundException {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        remove(task);
        return task;
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task: list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) throws TaskNotFoundException {
        try {
            return list.get(index);
        } catch(Exception e) {
            throw new TaskNotFoundException();
        }
    }

    @Override
    public List<Task> findAllByProjectId(String projectId) {
        final List<Task> projectTaskList = new ArrayList<>();
        for (final Task task: list) {
            if (projectId.equals(task.getProjectId())) projectTaskList.add(task);
        }
        return projectTaskList;
    }

}

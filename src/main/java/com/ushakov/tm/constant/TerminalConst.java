package com.ushakov.tm.constant;

public interface TerminalConst {

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_HELP = "help";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

    // Task Commands

    String CMD_TASK_CREATE = "task-create";

    String CMD_TASK_CLEAR = "task-clear";

    String CMD_TASK_LIST = "task-list";

    String CMD_FIND_TASK_BY_NAME = "find-task-by-name";

    String CMD_FIND_TASK_BY_ID = "find-task-by-id";

    String CMD_FIND_TASK_BY_INDEX = "find-task-by-index";

    String CMD_REMOVE_TASK_BY_NAME = "remove-task-by-name";

    String CMD_REMOVE_TASK_BY_ID = "remove-task-by-id";

    String CMD_REMOVE_TASK_BY_INDEX = "remove-task-by-index";

    String CMD_UPDATE_TASK_BY_ID = "update-task-by-id";

    String CMD_UPDATE_TASK_BY_INDEX = "update-task-by-index";

    String CMD_START_TASK_BY_INDEX = "start-task-by-index";

    String CMD_START_TASK_BY_ID = "start-task-by-id";

    String CMD_START_TASK_BY_NAME = "start-task-by-name";

    String CMD_COMPLETE_TASK_BY_INDEX = "complete-task-by-index";

    String CMD_COMPLETE_TASK_BY_ID = "complete-task-by-id";

    String CMD_COMPLETE_TASK_BY_NAME = "complete-task-by-name";

    String CMD_CHANGE_TASK_STATUS_BY_INDEX = "change-task-status-by-index";

    String CMD_CHANGE_TASK_STATUS_BY_ID = "change-task-status-by-id";

    String CMD_CHANGE_TASK_STATUS_BY_NAME = "change-task-status-by-name";

    String CMD_FIND_TASKS_BY_PROJECT_ID = "find-tasks-by-project-id";

    String CMD_BIND_TASK_BY_PROJECT_ID = "bind-task-by-project-id";

    String CMD_UNBIND_TASK_FROM_PROJECT = "unbind-task-from-project";

    //Project Commands

    String CMD_PROJECT_CREATE = "project-create";

    String CMD_PROJECT_CLEAR = "project-clear";

    String CMD_PROJECT_LIST = "project-list";

    String CMD_FIND_PROJECT_BY_NAME = "find-project-by-name";

    String CMD_FIND_PROJECT_BY_ID = "find-project-by-id";

    String CMD_FIND_PROJECT_BY_INDEX = "find-project-by-index";

    String CMD_REMOVE_PROJECT_BY_NAME = "remove-project-by-name";

    String CMD_REMOVE_PROJECT_BY_ID = "remove-project-by-id";

    String CMD_REMOVE_PROJECT_BY_INDEX = "remove-project-by-index";

    String CMD_UPDATE_PROJECT_BY_ID = "update-project-by-id";

    String CMD_UPDATE_PROJECT_BY_INDEX = "update-project-by-index";

    String CMD_START_PROJECT_BY_INDEX = "start-project-by-index";

    String CMD_START_PROJECT_BY_ID = "start-project-by-id";

    String CMD_START_PROJECT_BY_NAME = "start-project-by-name";

    String CMD_COMPLETE_PROJECT_BY_INDEX = "complete-project-by-index";

    String CMD_COMPLETE_PROJECT_BY_ID = "complete-project-by-id";

    String CMD_COMPLETE_PROJECT_BY_NAME = "complete-project-by-name";

    String CMD_CHANGE_PROJECT_STATUS_BY_INDEX = "change-project-status-by-index";

    String CMD_CHANGE_PROJECT_STATUS_BY_ID = "change-project-status-by-id";

    String CMD_CHANGE_PROJECT_STATUS_BY_NAME = "change-project-status-by-name";

    String CMD_DELETE_PROJECT_WITH_TASKS = "delete-project-with-tasks";

}

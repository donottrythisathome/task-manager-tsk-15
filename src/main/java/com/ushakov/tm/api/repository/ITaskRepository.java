package com.ushakov.tm.api.repository;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task removeOneByName(String name) throws TaskNotFoundException;

    Task removeOneById(String id) throws TaskNotFoundException;

    Task removeOneByIndex(Integer index) throws TaskNotFoundException;

    Task findOneByName(String name);

    Task findOneById(String id);

    Task findOneByIndex(Integer index) throws TaskNotFoundException;

}

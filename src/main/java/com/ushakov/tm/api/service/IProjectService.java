package com.ushakov.tm.api.service;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description) throws EmptyDescriptionException, EmptyNameException;

    void add(Project project) throws ProjectNotFoundException;

    void remove(Project project) throws ProjectNotFoundException;

    void clear();

    Project removeOneByName(String name) throws ProjectNotFoundException, EmptyNameException;

    Project removeOneById(String id) throws EmptyIdException, ProjectNotFoundException;

    Project removeOneByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException;

    Project findOneByName(String name) throws EmptyNameException;

    Project findOneById(String id) throws EmptyIdException;

    Project findOneByIndex(Integer index) throws EmptyIndexException, ProjectNotFoundException;

    Project updateProjectByIndex(Integer index, String name, String description) throws ProjectNotFoundException, EmptyNameException, EmptyIndexException;

    Project updateProjectById(String id, String name, String description) throws ProjectNotFoundException, EmptyNameException, EmptyIdException;

    Project startProjectById(String id) throws ProjectNotFoundException, EmptyIdException;

    Project startProjectByName(String name) throws ProjectNotFoundException, EmptyNameException;

    Project startProjectByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException;

    Project completeProjectById(String id) throws ProjectNotFoundException, EmptyIdException;

    Project completeProjectByName(String name) throws ProjectNotFoundException, EmptyNameException;

    Project completeProjectByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException;

    Project changeProjectStatusById(String id, Status status) throws ProjectNotFoundException, EmptyIdException;

    Project changeProjectStatusByName(String name, Status status) throws ProjectNotFoundException, EmptyNameException;

    Project changeProjectStatusByIndex(Integer index, Status status) throws ProjectNotFoundException, EmptyIndexException;

}

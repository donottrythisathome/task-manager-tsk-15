package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.IProjectRepository;

import com.ushakov.tm.api.service.IProjectService;
import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if(comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description)
            throws EmptyDescriptionException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project removeOneByName(final String name) throws ProjectNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project removeOneById(final String id) throws EmptyIdException, ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(final Integer index) throws ProjectNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) throws EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project findOneById(final String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) throws EmptyIndexException, ProjectNotFoundException {
        if (index == null || index < 0) throw new EmptyIndexException();
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description)
            throws ProjectNotFoundException, EmptyNameException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description)
            throws ProjectNotFoundException, EmptyNameException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(String id) throws ProjectNotFoundException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(String name) throws ProjectNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project completeProjectById(String id) throws ProjectNotFoundException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project completeProjectByName(String name) throws ProjectNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project completeProjectByIndex(Integer index) throws ProjectNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeProjectStatusById(String id, Status status) throws ProjectNotFoundException, EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(String name, Status status)
            throws ProjectNotFoundException, EmptyNameException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(Integer index, Status status)
            throws ProjectNotFoundException, EmptyIndexException {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
